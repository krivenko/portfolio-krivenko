<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package default-theme
 */

get_header(); ?>

    <section class="archive">
        <?php if ( have_posts() ) : ?>

            <?php
                while ( have_posts() ) : the_post();

    		        get_template_part( 'template-parts/content', get_post_format() );

                endwhile;
            ?>

        <?php
            else :

                get_template_part( 'template-parts/content', 'none' );

            endif;
        ?>
    </section>

<?php get_footer(); ?>
