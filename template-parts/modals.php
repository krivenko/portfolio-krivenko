
<!-- Modal - Success -->
<div class="modal fade" id="modalSuccess" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog w-500" role="document">
        <div class="modal-content">
            <div class="top">
                <button type="button" class="modal-close" data-dismiss="modal"></button>
                <h4>Сообщение отправлено!</h4>
                <p>Я свяжусь с вами в самое ближайшее время</p>
            </div>
        </div>
    </div>
</div>