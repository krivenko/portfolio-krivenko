<footer class="footer">
	<div class="main-block">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="inner">
						<nav class="footer-menu">
							<?php wp_nav_menu( 
								array(
									'menu'       => 'Top menu',
									'container'       => '',
									'container_class' => '',
									'menu_class' => '',
									'menu_id'         => 'footer-menu',
									'theme_location'  => 'top-menu'
								)
							); ?>
						</nav>
						<nav class="footer-socials">
							<a href="https://vk.com/dimakrivenko" class="vkontakte" target="_blank"></a>
							<a href="https://t.me/krivenko" class="telegram" target="_blank"></a>
							<a href="https://instagram.com/krivenko.di" class="instagram" target="_blank"></a>
						</nav>
					</div>
				</div>
			</div>
		</div>
	</div>
	<p class="copyright">© <?php echo '2011-' . date('Y') ?> - Кривенко Дмитрий</p>
</footer>
