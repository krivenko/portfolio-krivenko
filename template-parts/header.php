<header class="header">
	<a class="logo" href="<?php echo home_url( '/' ); ?>">
		<img src="<?php bloginfo('template_url'); ?>/assets/images/logo.png">
		<div class="text">
			<p class="big">Дмитрий Кривенко</p>
			<p>Front-end Developer</p>
		</div>
	</a>
	<nav class="top-menu">
		<?php wp_nav_menu( 
			array(
				'menu'       => 'Top menu',
				'container'       => '',
				'container_class' => '',
				'menu_class' => '',
				'menu_id'         => 'top-menu',
				'theme_location'  => 'top-menu'
			)
		); ?>
		<div class="close-menu"></div>
	</nav>
	<div class="menu-toggle">
		<i></i>
		<i></i>
		<i></i>
	</div>
</header>
