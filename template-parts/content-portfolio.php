<?php
/**
 * content-portfolio.php
 *
 * portfolio page content
 */

$portfolio_screenshots = fw_get_db_post_option(get_the_ID(), 'portfolio_screenshots');
$portfolio_link = fw_get_db_post_option(get_the_ID(), 'portfolio_link');
$portfolio_comp_works = fw_get_db_post_option(get_the_ID(), 'portfolio_comp_works');
?>

<section id="post-<?php the_ID(); ?>" <?php post_class('single-project'); ?>>
	<div class="content-main">

		<?php if( '' !== get_post()->post_content ) : ?>
			<div class="description">
				<?php the_content(); ?>
			</div>
		<?php endif; ?>

		<?php if (count($portfolio_screenshots) > 0) : ?>
			<div class="image-list">
				<?php foreach ($portfolio_screenshots as $key => $value) : ?>
					<div class="item">
						<img class="lazy" data-src="<?php esc_attr_e($value['url']); ?>">
					</div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>

		<nav class="page-pagination">
			<?php
				wp_link_pages( array(
					'before' => '<div class="page-links">' . esc_html__( 'Pages:', '_s' ),
					'after'  => '</div>',
				) );
			?>
		</nav>
	</div>
	<div class="content-aside">
		<?php if (has_post_thumbnail() == true) : ?>
			<div class="company-logo">
				<img class="lazy" data-src="<?php esc_attr_e(get_the_post_thumbnail_url()); ?>">
			</div>
		<?php endif; ?>

		<?php if ($portfolio_comp_works != '') : ?>
			<div class="text">
				<p><span>Выполненные работы:</span> <?php esc_html_e($portfolio_comp_works); ?></p>
			</div>
		<?php endif; ?>

		<?php if ($portfolio_link != '') : ?>
			<a href="<?php print(esc_url($portfolio_link)); ?>" class="btn primary" target="_blank">Посмотреть сайт</a>
		<?php endif; ?>
	</div>
</section><!-- #post-## -->
