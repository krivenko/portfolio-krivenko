
<?php
/**
 * content-portfolio-list.php
 */
?>


<!-- <a class="item animation" href="<?php the_permalink(); ?>" data-animation-name="fadeIn"> -->
<a class="item animation" id="post-<?php the_ID(); ?>" href="<?php the_permalink(); ?>">
	<div class="inner">
		<div class="image lazy" data-src="<?php echo (has_post_thumbnail() == true) ? get_the_post_thumbnail_url(get_the_ID(), 'full') : bloginfo('template_url').'/assets/images/no-image.jpg'; ?>"></div>
		<p class="text"><?php the_title(); ?></p>
	</div>
</a>

