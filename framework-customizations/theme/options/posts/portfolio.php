<?php
	if ( ! defined( 'FW' ) ) {
		die( 'Forbidden' );
	}

	$options = array(
		'portfolio_boxs' => array(
			'title' => 'Параметры работы',
			'type' => 'box',
			'options' => array(
				'portfolio_comp_works' => array(
					'label' => __('Выполненные работы', 'portfolio_krivenko'),
					'type' => 'text',
					'value' => ''
				),
				'portfolio_link' => array(
					'label' => __('Ссылка на сайт', 'portfolio_krivenko'),
					'type' => 'text',
					'value' => ''
				),
				'portfolio_screenshots' => array(
					'label' => __('Скриншоты', 'portfolio_krivenko'),
					'type'  => 'multi-upload',
					'value' => '',
					'images_only' => true,
				)
			)
		)
	);