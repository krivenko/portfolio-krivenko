<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package default-theme
 */

get_header();
    
    if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

        <section class="l-page">
            <section class="page-header">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="inner">
                                <div class="text">
                                    <h1><?php the_title(); ?></h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="page-content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <?php get_template_part( 'template-parts/content', get_post_type() ); ?>
                        </div>
                    </div>
                </div>
            </section>
        </section>

    <?php endwhile; endif; ?>


<?php get_footer(); ?>
