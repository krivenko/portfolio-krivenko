<?php
    if (!defined('ABSPATH')) {
        die('Direct access forbidden.');
    }

    /**
     * Theme’s ajax
     */

    function sendEmail ($postArr) {
        // $headers .= 'From: '.get_bloginfo('name').' <no-reply@'.preg_replace('#^https?://#', '', get_option('siteurl')).'>' . "\r\n";
        
        $headers .= 'From: '.get_bloginfo('name').' <no-reply@test.ru>' . "\r\n"; // Remove this shit
        
        $headers .= 'MIME-Version: 1.0' . "\r\n";
        $headers .= "Content-Type: text/html;\r\n";

        $recipients = 'wen2333@gmail.com';

        $subject = __('Сообщение с сайта - ', 'portfolio_krivenko' ) . ' ' . get_bloginfo('name');
        $message_body = '<h2>'.$subject.'</h2>';

        foreach ($postArr as $key => $value) {
            if ($key != 'privacy' && $key != 'ya_metrica_goal_name' && $key != 'action' && $key != 'form_name') {
                $message_body .= "<p><b>".$key." : </b>".$value."</p>";
            }
            if ($key == 'form_name') {
                $message_body .= "<p><b>".__('Form name', '')." : </b>".$value."</p>";
            }
        }

        var_dump($subject);

        wp_mail($recipients, $subject, $message_body, $headers);
    }

    // Send message to email
    add_action( 'wp_ajax_send_message', 'portfolio_krivenko_send_message_callback' ); // For logged in users
    add_action( 'wp_ajax_nopriv_send_message', 'portfolio_krivenko_send_message_callback' ); // For anonymous users



    function portfolio_krivenko_send_message_callback () {
        // Send on email
        sendEmail($_POST);

        wp_die();
    }


    function portfolio_krivenko_portfolio_loadmore(){
     
        $args = unserialize( stripslashes( $_POST['query'] ) );
        $args['paged'] = $_POST['page'] + 1; // следующая страница
        $args['post_status'] = 'publish';


     
        // обычно лучше использовать WP_Query, но не здесь

        $posts = query_posts( $args );

        // если посты есть
        if( count($posts) > 0 ) {
            foreach ($posts as $key => $post) : ?>
                <a class="item animation animated fadeIn" id="post-<?php echo $post->ID; ?>" href="<?php echo get_the_permalink($post->ID); ?>">
                    <div class="inner">
                        <div class="image" style="background-image: url(<?php echo (has_post_thumbnail($post->ID) == true) ? get_the_post_thumbnail_url($post->ID, 'full') : bloginfo('template_url').'/assets/images/no-image.jpg'; ?>)"></div>
                        <p class="text"><?php echo get_the_title($post->ID); ?></p>
                    </div>
                </a>
            <?php endforeach; ?><?php
                // var_dump($post);
                // ;
                
                // 
        }
     

        die();
    }
     
     
    add_action('wp_ajax_portfolio_loadmore', 'portfolio_krivenko_portfolio_loadmore');
    add_action('wp_ajax_nopriv_portfolio_loadmore', 'portfolio_krivenko_portfolio_loadmore');
