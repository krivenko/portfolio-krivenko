<?php
    if (!defined('ABSPATH')) {
        die('Direct access forbidden.');
    }

    /**
     * Theme’s helpers
     */

    // Support SVG mime types
    function cc_mime_types($mimes) {
        $mimes['svg'] = 'image/svg+xml';
        return $mimes;
    }
    add_filter('upload_mimes', 'cc_mime_types');


    // Get meta option from builder
    function get_meta_builder( $id, $shortcodeName ) {
        $meta = get_post_meta( absint( $id ), 'fw:opt:ext:pb:page-builder:json', true );
        $result = json_decode( $meta );

        // var_dump($result);

        foreach ( $result as $stdClass ) {
            // var_dump($stdClass->shortcode);
            if( $stdClass instanceof stdClass ) {
                if( $stdClass->shortcode === $shortcodeName ) {
                    return $stdClass->atts;
                }
            }
        }

        return null;
    }
