<?php

    if (!defined('ABSPATH')) {
        die('Direct access forbidden.');
    }

    /**
     * Theme’s static files
     */

    if (!is_admin()) {
        wp_enqueue_style('main', PORTFOLIO_KRIVENKO_CSS . '/main.css', null, PORTFOLIO_KRIVENKO_VERSION);
    }

    if (!is_admin()) {
        // wp_enqueue_script('jquery');
        // wp_deregister_script( 'jquery' );
        // wp_register_script( 'jquery', includes_url( '/js/jquery/jquery.js' ), false, NULL, true );
        wp_enqueue_script( 'jquery' );
        // wp_enqueue_script('popper', PORTFOLIO_KRIVENKO_SCRIPTS . '/popper.js', array('jquery'), PORTFOLIO_KRIVENKO_VERSION, true);
        // wp_enqueue_script('bootstrap', PORTFOLIO_KRIVENKO_SCRIPTS . '/bootstrap.js', array('jquery'), PORTFOLIO_KRIVENKO_VERSION, true);
        // wp_enqueue_script('bootstrap-validator', PORTFOLIO_KRIVENKO_SCRIPTS . '/bootstrap-validator.js', array('jquery'), PORTFOLIO_KRIVENKO_VERSION, true);
        // wp_enqueue_script('lazyload', PORTFOLIO_KRIVENKO_SCRIPTS . '/jquery.lazyload.min.js', array('jquery'), PORTFOLIO_KRIVENKO_VERSION, true);
        // wp_enqueue_script('maskedinput', PORTFOLIO_KRIVENKO_SCRIPTS . '/jquery.maskedinput.min.js', array('jquery'), PORTFOLIO_KRIVENKO_VERSION, true);
        // wp_enqueue_script('slick', PORTFOLIO_KRIVENKO_SCRIPTS . '/slick.min.js', array('jquery'), PORTFOLIO_KRIVENKO_VERSION, true);
        // wp_enqueue_script('countdown-timer', PORTFOLIO_KRIVENKO_SCRIPTS . '/countdown-timer.js', array('jquery'), PORTFOLIO_KRIVENKO_VERSION, true);
        // wp_enqueue_script('waypoints', PORTFOLIO_KRIVENKO_SCRIPTS . '/jquery.waypoints.min.js', array('jquery'), PORTFOLIO_KRIVENKO_VERSION, true);
        wp_enqueue_script('libs', PORTFOLIO_KRIVENKO_SCRIPTS . '/libs.js', array('jquery'), PORTFOLIO_KRIVENKO_VERSION, true);
        wp_enqueue_script('main', PORTFOLIO_KRIVENKO_SCRIPTS . '/main.js', array('jquery'), PORTFOLIO_KRIVENKO_VERSION, true);
        wp_enqueue_script('custom', PORTFOLIO_KRIVENKO_SCRIPTS . '/custom.js', array('jquery'), PORTFOLIO_KRIVENKO_VERSION, true);
    }
