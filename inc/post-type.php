<?php
    if (! defined('ABSPATH')) {
        die('Direct access forbidden.');
    }

	/**
	 * Post Type: Portfolio.
	 */

	$portfolioLabels = array(
		"name" => __( "Портфолио", "portfolio_krivenko" ),
		"singular_name" => __( "Портфолио", "portfolio_krivenko" ),
		"menu_name" => __( "Портфолио", "portfolio_krivenko" ),
		"all_items" => __( "Все работы", "portfolio_krivenko" ),
		"add_new" => __( "Добавить", "portfolio_krivenko" ),
		"add_new_item" => __( "Добавить", "portfolio_krivenko" ),
		"edit_item" => __( "Изменить", "portfolio_krivenko" ),
		"new_item" => __( "Добавить", "portfolio_krivenko" ),
		"view_item" => __( "Посмотреть", "portfolio_krivenko" ),
		"view_items" => __( "Посмотреть все", "portfolio_krivenko" ),
	);

	$portfolioArgs = array(
		"label" => __( "Portfolio", "portfolio_krivenko" ),
		"labels" => $portfolioLabels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"delete_with_user" => false,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "portfolio", "with_front" => true ),
		"query_var" => true,
		"menu_icon" => "dashicons-format-image",
		"supports" => array( "title", "editor", "thumbnail" ),
		"taxonomies" => array( "category" ),
	);

	register_post_type( "portfolio", $portfolioArgs );





