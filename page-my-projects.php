<?php
/**
 * Template Name: My projects
 *
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package default-theme
 */

get_header();

   if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

        <section class="l-page my-projects">
            <section class="page-header">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="inner">
                                <div class="text">
                                    <h1><?php the_title(); ?></h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="page-content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="inner">
                                <?php if ( !empty( get_the_content() ) ) : ?>
                                    <div class="description">
                                        <?php the_content(); ?>
                                    </div>
                                <?php endif; ?>
                                <div class="my-projects-list">
                                    <div class="item">
                                        <h4>AviaMonitor - Дешевые авиабилеты</h4>
                                        <p>Телеграм канал с подборками выгодных цен на авиабилеты, лайфхаками, и новостями для путешественников</p>
                                        <a href="https://tele.gg/aviamonitor" class="more" target="_blank">Посмотреть</a>
                                    </div>
                                    <div class="item">
                                        <h4>AviaMonitorBot</h4>
                                        <p>Telegram чат-бот для поиска и покупки дешевых авиабилетов на Aviasales</p>
                                        <a href="https://tele.gg/aviamonitorbot" class="more" target="_blank">Посмотреть</a>
                                    </div>
                                    <div class="item">
                                        <h4>Digital-агенство - "boxAgency"</h4>
                                        <p>Агенство занимается разработкой и продвижением landing-page</p>
                                        <a href="https://boxagency.ru/" class="more" target="_blank">Посмотреть</a>
                                    </div>
                                    <div class="item">
                                        <h4>Группа в ВК - "Стругацкие - цитаты из книг"</h4>
                                        <p>В этой группе размещаются цитаты из произведений братьев Стругацких</p>
                                        <a href="https://vk.com/strugatsky_quote" class="more" target="_blank">Посмотреть</a>
                                    </div>
                                    <div class="item">
                                        <h4>Группа в ВК - "Hot Sticker - Виниловые стикеры"</h4>
                                        <p>В этой группе продаются различные виниловые стикеры с классными дизайнами</p>
                                        <a href="https://vk.com/hot_sticker" class="more" target="_blank">Посмотреть</a>
                                    </div>
                                    <div class="item">
                                        <h4>Instagram - "Объявления в Питере"</h4>
                                        <p>На этой странице размещаются объявления по городу Санкт-Петербург</p>
                                        <a href="https://www.instagram.com/obyavleniya_piter/" class="more" target="_blank">Посмотреть</a>
                                    </div>
                                    <div class="item">
                                        <h4>Telegram - "Бот для бросания игральных костей"</h4>
                                        <p>С помощью этого бота вы сможете бросать игральные кости когда их нет под рукой</p>
                                        <a href="http://t-do.ru/random_dice_bot" class="more" target="_blank">Посмотреть</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </section>

    <?php endwhile; endif; ?>
<?php get_footer(); ?>