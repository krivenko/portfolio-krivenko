<?php
/**
 * Template Name: Homepage 
 *
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package default-theme
 */

get_header(); ?>

    <section class="l-page home">
		<section class="page-header">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="inner">
							<div class="text">
								<h1>Разработка сайтов</h1>
								<p>Современные адаптивные сайты с интуитивным дизайном</p>
								<div class="view-portfolio">
									<a href="#portfolio_list" class="scroll-to">Посмотреть портфолио</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="portfolio-list" id="portfolio_list">
			<div class="container">
				<div class="row">
					<div class="portfolio-list-inner">
						<?php
							$query = new WP_Query( array( 'post_type' => 'portfolio' ) );
							if ($query->posts) : 
							foreach ($query->posts as $post) : setup_postdata ($post); ?>
							<?php get_template_part( 'template-parts/content-portfolio-list'); ?>
						<?php endforeach; endif; ?>
					</div>

					<?php if (  $query->max_num_pages > 1 ) : ?>
						<script>
							var ajaxurl = '<?php echo site_url() ?>/wp-admin/admin-ajax.php';
							var true_posts = '<?php echo serialize($query->query_vars); ?>';
							var current_page = <?php echo (get_query_var('paged')) ? get_query_var('paged') : 1; ?>;
							var max_pages = '<?php echo $query->max_num_pages; ?>';
						</script>
						<div class="loadmore">
							<button class="btn primary">Показать ещё</button>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</section>

		<section class="homepage-contact-form">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="block-title">
							<h4>Остались вопросы?</h4>
							<p>Оставьте заявку, для получения бесплатной консультации</p>
						</div>
						<form class="common-form" data-toggle="validator" role="form" data-focus="false" novalidate="true">
                            <div class="inner">
	                            <div class="form-group">
	                                <input type="text" name="phone" class="form-control" placeholder="+7(___)-___-__-__" required>
	                                <small class="err">Введите имя!</small>
	                             </div>
	                            <button class="btn primary"><span>Отправить</span></button>
                            </div>
                            <div class="privacy">
                                <label class="checkbox">
                                    <input type="checkbox" name="privacy" checked="">
                                    <i></i>
                                </label>
                                <p>Я даю согласие на обработку персональных данных и соглашаюсь c <a href="<?php echo home_url( '/privacy' ); ?>" target="_blank">политикой конфиденциальности</a></p>
                            </div>

                            <input type="hidden" name="action" value="send_message">
                            <input type="hidden" name="form_name" value="Заявка на звонок - Главная страница">
                            <input type="hidden" name="ya_metrica_goal_name" value="form_contacts">
                        </form>
					</div>
				</div>
			</div>
		</section>
    </section>

<?php get_footer(); ?>