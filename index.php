<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package default-theme
 */

get_header(); ?>

    <section class="l-page home">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <?php if ( have_posts() ) : ?>

                        <?php
                            while ( have_posts() ) : the_post();

                		        get_template_part( 'template-parts/content', get_post_format() );

                            endwhile;
                        ?>

                    <?php
                        else :

                            get_template_part( 'template-parts/content', 'none' );

                        endif;
                    ?>
                 </div>
            </div>
        </div>
    </section>

<?php get_footer(); ?>
