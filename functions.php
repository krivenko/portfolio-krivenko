<?php
    /**
     * functions.php
     *
     * The theme's functions and definitions.
     */
    /**
     * Define constants. Current Version number & Theme Name.
     */

    define('PORTFOLIO_KRIVENKO_THEME', 'Portfolio Krivenko');
    define('PORTFOLIO_KRIVENKO_VERSION', '1.0.0');
    define('PORTFOLIO_KRIVENKO_THEMEROOT', get_template_directory_uri());
    define('PORTFOLIO_KRIVENKO_ASSETS', PORTFOLIO_KRIVENKO_THEMEROOT . '/assets');
    define('PORTFOLIO_KRIVENKO_IMAGES', PORTFOLIO_KRIVENKO_THEMEROOT . '/assets/images');
    define('PORTFOLIO_KRIVENKO_CSS', PORTFOLIO_KRIVENKO_THEMEROOT . '/assets/css');
    define('PORTFOLIO_KRIVENKO_SCRIPTS', PORTFOLIO_KRIVENKO_THEMEROOT . '/assets/js');

    /**
     * ----------------------------------------------------------------------------------------
     * Set up the content width value based on the theme's design.
     * ----------------------------------------------------------------------------------------
     */
    if (!isset($content_width)) {
        $content_width = 800;
    }

    /**
     * ----------------------------------------------------------------------------------------
     * Set up theme default and register various supported features.
     * ----------------------------------------------------------------------------------------
     */
    if (!function_exists('portfolio_krivenko_setup')) {
        function portfolio_krivenko()
        {
            /**
             * Make the theme available for translation.
             */
            load_theme_textdomain('portfolio_krivenko', get_template_directory() . '/languages');


            /**
             * Add support for post formats.
             */
            // add_theme_support('post-formats', array()
            // );

            /**
             * Add support for automatic feed links.
             */
            add_theme_support('automatic-feed-links');

            /*
             * Let WordPress manage the document title.
             * By adding theme support, we declare that this theme does not use a
             * hard-coded <title> tag in the document head, and expect WordPress to
             * provide it for us.
             */
            add_theme_support('title-tag');


            /**
             * Add support for post thumbnails.
             */
            add_theme_support('post-thumbnails');
            set_post_thumbnail_size(300, 300, array('center', 'center')); // Hard crop center center


            /**
             * Register nav menus.
             */
            register_nav_menus(
                array(
                    'primary' => __('Main menu', 'domain')
                )
            );
            /*
             * Switch default core markup for search form, comment form, and comments
             * to output valid HTML5.
             */
            add_theme_support('html5', array(
                'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
            ));

            add_filter('upload_mimes', 'cc_mime_types');
        }

        add_action('after_setup_theme', 'portfolio_krivenko');
    }

    /*
     * Theme Inc
     */
    include_once get_template_directory() .'/inc/init.php';

    /*
     * Castomizer
     */
    include_once get_template_directory() .'/inc/castomizer.php';

    /*
     * AJAX
     */
    include_once get_template_directory() .'/inc/ajax.php';
