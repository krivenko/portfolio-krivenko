<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package default-theme
 */

get_header(); ?>

    <section class="l-page page-404">
        <section class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="text">
                            <h1>404</h1>
                            <p>Извините, но этой страницы не существует</p>
                            <a href="<?php echo home_url( '/' ); ?>" class="btn">На главную</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>

<?php get_footer(); ?>
