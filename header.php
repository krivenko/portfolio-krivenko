<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package default-theme
 */
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <link rel="icon" type="image/png" href="<?php bloginfo('template_url'); ?>/assets/images/favicon.png">

    <meta property="og:title" content="Дмитрий Кривенко - Разработка сайтов" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="<?php echo get_permalink(); ?>" />
    <meta property="og:image" content="<?php bloginfo('template_url'); ?>/assets/images/open-graph-logo.jpg" />

    <?php wp_head(); ?>
</head>

<body <?php body_class('init-animation animation-transition'); ?>>
    <section class="l-wrapper">
        <section class="l-main">
        	<?php get_template_part( 'template-parts/header' ); ?>
