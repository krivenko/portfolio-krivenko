<?php
/**
 * Template Name: Portfolio
 *
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package default-theme
 */

get_header();

   if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

        <section class="l-page portfolio">
            <section class="page-header">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="inner">
                                <div class="text">
                                    <h1><?php the_title(); ?></h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="page-content">
                <section class="portfolio-list" id="portfolio_list">
                    <div class="container">
                        <div class="row">
                            <div class="portfolio-list-inner">
                                <?php
                                    $query = new WP_Query( array( 'post_type' => 'portfolio', 'posts_per_page' => 999, ) );
                                    if ($query->posts) : 
                                    foreach ($query->posts as $post) : setup_postdata ($post); ?>
                                    <?php get_template_part( 'template-parts/content-portfolio-list'); ?>
                                <?php endforeach; endif; ?>
                            </div>
                        </div>
                    </div>
                </section>
            </section>
        </section>

    <?php endwhile; endif; ?>
<?php get_footer(); ?>