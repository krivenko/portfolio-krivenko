<?php
/**
 * Template Name: Contacts
 *
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package default-theme
 */

get_header();

   if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

        <section class="l-page contacts">
            <section class="page-header">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="inner">
                                <div class="text">
                                    <h1><?php the_title(); ?></h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="page-content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <?php the_content(); ?>

                            <div class="row">
                                <div class="col-md-5 col-lg-6">
                                    <div class="block-title">
                                        <h4>Контактная информация</h4>
                                        <p>Если у вас есть что-то обсудить по вашему проекты или вы хотите, узнать стоимость работ, не стесняйтесь связаться со мной любым удобным способом.</p>
                                    </div>
                                    <div class="contact-list">
                                        <div class="item">
                                            <p>Телефон</p>
                                            <a href="tel:+79887680205">+7 (988) 768-02-05</a>
                                        </div>
                                        <div class="item">
                                            <p>Почта</p>
                                            <a href="mailto:wen2333@gmail.com">wen2333@gmail.com</a>
                                        </div>
                                        <div class="item">
                                            <p>Вконтакте</p>
                                            <a href="https://vk.com/dimakrivenko" target="_blank">vk.com/dimakrivenko</a>
                                        </div>
                                        <div class="item">
                                            <p>Telegram</p>
                                            <a href="https://t.me/krivenko" target="_blank">@krivenko</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-7 col-lg-6">
                                    <form class="common-form" data-toggle="validator" role="form" data-focus="false" novalidate="true">
                                        <div class="block-title">
                                            <h4>Остались вопросы? Оставьте их здесь</h4>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" name="name" class="form-control" placeholder="Имя">
                                            <small class="err">Введите имя!</small>
                                         </div>
                                        <div class="form-group">
                                            <input type="text" name="email" class="form-control" placeholder="E-mail" required>
                                            <small class="err">Введите email!</small>
                                        </div>
                                        <div class="form-group">
                                            <textarea name="message" class="form-control" placeholder="Текст сообщения"></textarea>
                                            <small class="err">Введите сообщение!</small>
                                        </div>
                                        <button class="btn primary"><span>Отправить</span></button>
                                        <div class="privacy">
                                            <label class="checkbox">
                                                <input type="checkbox" name="privacy" checked="">
                                                <i></i>
                                            </label>
                                            <p>Я даю согласие на обработку персональных данных и соглашаюсь c <a href="<?php echo home_url( '/privacy' ); ?>" target="_blank">политикой конфиденциальности</a></p>
                                        </div>

                                        <input type="hidden" name="action" value="send_message">
                                        <input type="hidden" name="form_name" value="Страница - Контакты">
                                        <input type="hidden" name="ya_metrica_goal_name" value="form_contacts">
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </section>

    <?php endwhile; endif; ?>
<?php get_footer(); ?>
