/*! jquery.cookie v1.4.1 | MIT */
!function(a){"function"==typeof define&&define.amd?define(["jquery"],a):"object"==typeof exports?a(require("jquery")):a(jQuery)}(function(a){function b(a){return h.raw?a:encodeURIComponent(a)}function c(a){return h.raw?a:decodeURIComponent(a)}function d(a){return b(h.json?JSON.stringify(a):String(a))}function e(a){0===a.indexOf('"')&&(a=a.slice(1,-1).replace(/\\"/g,'"').replace(/\\\\/g,"\\"));try{return a=decodeURIComponent(a.replace(g," ")),h.json?JSON.parse(a):a}catch(b){}}function f(b,c){var d=h.raw?b:e(b);return a.isFunction(c)?c(d):d}var g=/\+/g,h=a.cookie=function(e,g,i){if(void 0!==g&&!a.isFunction(g)){if(i=a.extend({},h.defaults,i),"number"==typeof i.expires){var j=i.expires,k=i.expires=new Date;k.setTime(+k+864e5*j)}return document.cookie=[b(e),"=",d(g),i.expires?"; expires="+i.expires.toUTCString():"",i.path?"; path="+i.path:"",i.domain?"; domain="+i.domain:"",i.secure?"; secure":""].join("")}for(var l=e?void 0:{},m=document.cookie?document.cookie.split("; "):[],n=0,o=m.length;o>n;n++){var p=m[n].split("="),q=c(p.shift()),r=p.join("=");if(e&&e===q){l=f(r,g);break}e||void 0===(r=f(r))||(l[q]=r)}return l};h.defaults={},a.removeCookie=function(b,c){return void 0===a.cookie(b)?!1:(a.cookie(b,"",a.extend({},c,{expires:-1})),!a.cookie(b))}});



// CountdownTimer
// 
// Arguments
// 1. timer_date - date to timer end
// 2. timer_type - random, to_date
// 3. timer_days - count random days

var CountdownTimer = function(timer_date, timer_type, timer_days) {
    function CountdownTimer(elm_id,tl,mes){
        this.initialize.apply(this,arguments);
    }
    function getRandomInt(min, max)
    {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    CountdownTimer.prototype={
        initialize:function(elm_id,tl,mes) {
            this.elem = document.getElementById(elm_id);
            this.elem_days = jQuery('.timer.days');
            this.elem_hours = jQuery('.timer.hours');
            this.elem_minutes = jQuery('.timer.minutes');
            this.elem_seconds = jQuery('.timer.seconds');

            this.elem_label_days = jQuery('.label.days');
            this.elem_label_hours = jQuery('.label.hours');
            this.elem_label_minutes = jQuery('.label.minutes');
            this.elem_label_seconds = jQuery('.label.seconds');

            this.tl = tl;
            this.mes = mes;

            this.countDown();
        },countDown:function(){
            var today=new Date();
            var day=Math.floor((this.tl-today)/(24*60*60*1000));
            var hour=Math.floor(((this.tl-today)%(24*60*60*1000))/(60*60*1000));
            var min=Math.floor(((this.tl-today)%(24*60*60*1000))/(60*1000))%60;
            var sec=Math.floor(((this.tl-today)%(24*60*60*1000))/1000)%60%60;
            var me=this;
            if( ( this.tl - today ) > 0 ){
                this.elem_days.text( this.addZero(day) );
                this.elem_hours.text( this.addZero(hour) );
                this.elem_minutes.text( this.addZero(min) );
                this.elem_seconds.text( this.addZero(sec) );

                this.elem_label_days.text( this.plural_str(day,'день','дня','дней') );
                this.elem_label_hours.text( this.plural_str(hour,'час','часа','часов') );
                this.elem_label_minutes.text( this.plural_str(min,'минута','минуты','минут') );
                this.elem_label_seconds.text( this.plural_str(sec,'секунда','секунды','секунд') );
                var tid = setTimeout( function(){me.countDown();},500 );
                //tid = setTimeout( function(){me.countDown();},500 ); // old
            }
            else
            {
                
                return;
            }
        },addZero:function(num){ return ('0'+num).slice(-2); },
        plural:function (a){
            if ( a % 10 == 1 && a % 100 != 11 ) return 0
            else if ( a % 10 >= 2 && a % 10 <= 4 && ( a % 100 < 10 || a % 100 >= 20)) return 1
            else return 2;
        },
        plural_str:function (i, str1, str2, str3){
            switch (this.plural(i)) {
                case 0: return str1;
                case 1: return str2;
                default: return str3;
            }
        }
    }

    function CDT(){
        if (timer_type == 'random') {
            var dateDown = jQuery.cookie('cdt');
            var now = new Date();
            if (!dateDown || dateDown < now.getTime() && timer_days)
            {
                timer_days = parseInt(timer_days);
                dateDown = now.getTime() + timer_days*24*60*60*1000 + getRandomInt(1,4)*60*60*1000 - getRandomInt(1,4)*60*60*1000 - getRandomInt(1,26)*60*1000 + getRandomInt(1,26)*1000;
                jQuery.cookie('cdt',dateDown, { expires: timer_days , path: '/' });
            }
            dateDown = new Date( parseInt(dateDown) );
        }
        if (timer_type == 'to_date') {
            dateDown = new Date( timer_date  );
        }

        var tl = new Date(dateDown);
        var timer = new CountdownTimer('timer', tl, '');
    }
    CDT()
}
