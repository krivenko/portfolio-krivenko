(function($) {

    // Global vars
    var didScroll;
    var lastScrollTop = 0;
    var delta = 5;
    var navbarHeight = $('header').outerHeight();

    jQuery(document).ready(function($) {

        // Init animation
        (function(){
            setTimeout(function() {
                $('body').removeClass('init-animation');
            }, 200);
            setTimeout(function() {
                $('body').removeClass('animation-transition');
            }, 1200);
        })();

        // Animation on scroll
        $('.animation').each(function(){
            const self = $(this),
                animationName = $(this).data('animation-name') !== undefined ? $(this).data('animation-name') : 'fadeIn',
                animationDelay = $(this).data('animation-delay') !== undefined ? $(this).data('animation-delay') : 0,
                animationDuration = $(this).data('animation-duration') !== undefined ? $(this).data('animation-duration') : 1000,
                animationOffset = $(this).data('animation-offset') !== undefined ? $(this).data('animation-offset') : '100%';

            if (animationDuration !== 1000) {
                self.css('animation-duration', (animationDuration / 1000) + 's');
            }
        
            self.waypoint(function() {
                setTimeout(function() {
                    self.addClass('animated ' + animationName);
                }, animationDelay);
            }, { offset: animationOffset});
        });

        // Scroll to
        $('.scroll-to').click(function(e){
            e.preventDefault();

            var el = $(this).attr('href');

            console.log(el)

            $('html').animate({
                scrollTop: $(el).offset().top
            }, 500);

            return false;
        });

        // Маска телефона
    	$("input.form-control[name=phone]").mask("+7 (999) 999-99-99");

    	// Lazy load image
    	$(".lazy").lazyload();
    	$(".lazy").lazyload({
            event : "sporty"
        });

        // Открытие мобильного меню
        $('.header .menu-toggle').click(function(){
            $('.header .top-menu').addClass('is-open');
            $('body').addClass('overflow');
        });

        // // Закрытие мобильного меню
        // $('.header .top-menu').append('<i class="close"></i>');
        $('.header .top-menu .close-menu').click(function(){
            $('.header .top-menu').removeClass('is-open');
            $('body').removeClass('overflow');
        });

        // AJAX подгрузка работ в портфолил
        $('.portfolio-list .loadmore .btn').click(function(){
            var self = $(this),
                data = {
                    'action': 'portfolio_loadmore',
                    'query': true_posts,
                    'page' : current_page
                };

            self.text('Загрузка...');

            $.ajax({
                url: ajaxurl,
                data: data,
                type: 'POST',
                success:function(data){
                    if( data ) { 
                        $('.portfolio-list .portfolio-list-inner').append(data);
                        self.text('Показать ещё');
                        current_page++;
                        if (current_page == max_pages) {
                            self.parent().remove();
                        }
                    } else {
                        self.parent().remove();
                    }
                }
            });
        });

        // // Слайдер отзывов
        // $('.l-testimonials .testimonials-slider').slick({
        //     slidesToShow: 1,
        //     slidesToScroll: 1,
        //     dots: true,
        // });

        // // Открытие модали и добавление данные
        // $('.open-modal').click(function(){
        //     var title = $(this).data('modal-title'),
        //         subtitle = $(this).data('modal-subtitle'),
        //         button = $(this).data('modal-button'),
        //         formName = $(this).data('modal-form-name'),
        //         formYaGoal = $(this).data('modal-ya-goal');

        //     $('#modalCallback .top h4').html(title);
        //     $('#modalCallback .top p').html(subtitle);
        //     $('#modalCallback .btn span').html(button);
        //     $('#modalCallback input[name=form_name]').val(formName);
        //     $('#modalCallback input[name=ya_metrica_goal_name]').val(formYaGoal);
        // });

        // // Закрытие модали
        // $('#modalCallback').on('hidden.bs.modal', function(){
        //     $('#modalCallback .top h4').html('');
        //     $('#modalCallback .top p').html('');
        //     $('#modalCallback .btn span').html('');
        //     $('#modalCallback input[name=form_name]').val('');
        //     $('#modalCallback input[name=ya_metrica_goal_name]').val('');
        // });

        // Парс url
        function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        };

        // Отправка формы
        $(".common-form").validator().on("submit", function(e) {
    		var form = $(this),
    			formData = form.serializeArray(),
    			formName = form.find('input[name="form_name"]').val(),
    			formGoal = form.find('input[name="ya_metrica_goal_name"]').val(),
    			formPrivacy = form.find('input[name="privacy"]').prop("checked");

            setTimeout(function () {
                form.find('.form-group').removeClass('has-error');
            }, 3000);

            formData.push({ name: 'from_site', value: window.location.href });

            if (formName !== undefined) {
                formData.push({ name: 'form_name', value: formName });
            }

            if (getUrlParameter('utm_source') !== undefined) {
                formData.push({ name: 'utm_source', value: getUrlParameter('utm_source') });
            }

            if (getUrlParameter('utm_medium') !== undefined) {
                formData.push({ name: 'utm_medium', value: getUrlParameter('utm_medium') });
            }

            if (getUrlParameter('utm_campaign') !== undefined) {
                formData.push({ name: 'utm_campaign', value: getUrlParameter('utm_campaign') });
            }

            if (getUrlParameter('utm_content') !== undefined) {
                formData.push({ name: 'utm_content', value: getUrlParameter('utm_content') });
            }

            if (getUrlParameter('utm_term') !== undefined) {
                formData.push({ name: 'utm_term', value: getUrlParameter('utm_term') });
            }


    		if (!e.isDefaultPrevented()) {
                e.preventDefault();

                if(formPrivacy) {
                    form.find('.btn').addClass('loading').prop("disabled",true);;
                    $.ajax({
                        type: "POST",
                        url: ajaxurl.url,
                        data: formData,
                        success: function(res) {
                            form.trigger('reset');
                            form.find('.btn').removeClass('loading').prop("disabled",false);
                            
                            if (formGoal !== undefined) {
                                // yaCounter46528974.reachGoal(formGoal);
                            }

                            $('#modalCallback').modal('hide');

                            setTimeout(function () {
                                $('#modalSuccess').modal('show');
                            }, 500);
                            setTimeout(function () {
                                // $('#modalSuccess').modal('hide');
                            }, 5000);
                        }
                    });
                    return false;
                } else {
                    alert('Без согласия на обработку данных я не могу принять заявку');
                }

    		}
    	});

    });


    // // Cобытия на скролл
    // $(window).scroll(function() {
    //     // Затемнение меню
    //     (function() {
    //         var el = $(".header");
    //         $(window).scrollTop() > 125
    //             ? el.addClass("is-black")
    //             : el.removeClass("is-black");
    //     })();

    //     didScroll = true;
    // });

    // // Скрытие/Показ меню
    // (function(){
    //     setInterval(function() {
    //         if (didScroll) {
    //             hasScrolled();
    //             didScroll = false;
    //         }
    //     }, 250);

    //     function hasScrolled() {
    //         var st = $(this).scrollTop();

    //         if(Math.abs(lastScrollTop - st) <= delta)
    //             return;

    //         if (st > lastScrollTop && st > navbarHeight){
    //             $('header').removeClass('nav-down').addClass('nav-up');
    //         } else {
    //             if(st + $(window).height() < $(document).height()) {
    //                 $('header').removeClass('nav-up').addClass('nav-down');
    //             }
    //         }
    //         lastScrollTop = st;
    //     }
    // })();


})(jQuery);
