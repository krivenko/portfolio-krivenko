<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package default-theme
 */

get_header();
    
    if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

        <section class="l-page single">
            <section class="page-header">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="inner">
                                <div class="text">
                                    <h1><?php the_title(); ?></h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="page-content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <?php get_template_part( 'template-parts/content', get_post_format() ); ?>
                        </div>
                    </div>
                </div>
            </section>
        </section>

    <?php endwhile; endif; ?>


<?php get_footer(); ?>
